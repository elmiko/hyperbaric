# Contributing

If you would like to propse a change to Hyperbaric, please make a fork of this repository, create a feature
branch for your change, and then create a merge request from your new branch.

## How it works
This project aims to follow the Kubernetes [Operator pattern](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/)

It uses [Controllers](https://kubernetes.io/docs/concepts/architecture/controller/) 
which provides a reconcile function responsible for synchronizing resources untile the desired state is reached on the cluster 

## Test It Out
1. Install the CRDs into the cluster:

```sh
make install
```

2. Run your controller (this will run in the foreground, so switch to a new terminal if you want to leave it running):

```sh
make run
```

**NOTE:** You can also run this in one step by running: `make install run`

## Modifying the API definitions
If you are editing the API definitions, generate the manifests such as CRs or CRDs using:

```sh
make manifests
```

**NOTE:** Run `make --help` for more information on all potential `make` targets

More information can be found via the [Kubebuilder Documentation](https://book.kubebuilder.io/introduction.html)


## Configuration

Hyperbaric uses a file for its runtime configuration. See the file
[controller_manager_config.yaml](config/manager/controller_manager_config.yaml)
for an example of this file.

If you need to add configuration data, the schematized version of the configuration
file lives in [hyperbaricconfig_types.go](pkg/api/v1alpha1/hyperbaricconfig_types.go).
You should update both the API schema Go file and the example file when adding
new information.

This type is **not** used as a CRD in Kubernetes, so the manifests do not need to be
regenerated after updating the code schema.
