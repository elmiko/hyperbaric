/*
Copyright 2022 Michael McCune.
*/

package main

import (
	"context"
	"flag"
	"os"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"k8s.io/klog/v2"
	"k8s.io/klog/v2/klogr"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/dynamic"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"

	v1alpha1 "gitlab.com/elmiko/hyperbaric/pkg/api/v1alpha1"
	"gitlab.com/elmiko/hyperbaric/pkg/controllers"
	"gitlab.com/elmiko/hyperbaric/pkg/scalers"
	//+kubebuilder:scaffold:imports
)

const (
	defaultScalerIntervalSeconds int64 = 15
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(v1alpha1.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme
}

func main() {
	var configFile string
	flag.StringVar(&configFile, "config", "", "Hyperbaric will loads its initial configuration from this file. "+
		"Omit this flag to use the default configuration values.")

	klog.InitFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(klogr.New())

	var err error
	ctrlConfig := v1alpha1.HyperbaricConfig{}
	options := ctrl.Options{Scheme: scheme}
	if configFile != "" {
		options, err = options.AndFrom(ctrl.ConfigFile().AtPath(configFile).OfKind(&ctrlConfig))
		if err != nil {
			setupLog.Error(err, "unable to load the hyperbaric config file")
			os.Exit(1)
		}
	}

	// Configure defaults
	if ctrlConfig.ScalerIntervalSeconds <= 0 {
		ctrlConfig.ScalerIntervalSeconds = defaultScalerIntervalSeconds
	}

	var managementConfig *rest.Config
	if len(ctrlConfig.ManagementKubeconfig) > 0 {
		if mc, err := clientcmd.BuildConfigFromFlags("", ctrlConfig.ManagementKubeconfig); err != nil {
			setupLog.Error(err, "unable to build management kubeconfig")
			os.Exit(1)
		} else {
			managementConfig = mc
		}
	} else {
		managementConfig = ctrl.GetConfigOrDie()
	}

	// TODO add workloadConfig when needed
	// var workloadConfig *rest.Config
	if len(ctrlConfig.WorkloadKubeconfig) > 0 {
		if _, err := clientcmd.BuildConfigFromFlags("", ctrlConfig.ManagementKubeconfig); err != nil {
			setupLog.Error(err, "unable to build workload kubeconfig")
			os.Exit(1)
		} else {
			// workloadConfig = wc
		}
	}

	mgr, err := ctrl.NewManager(managementConfig, options)
	if err != nil {
		setupLog.Error(err, "unable to start controller manager")
		os.Exit(1)
	}

	if err = (&controllers.NodeGroupReconciler{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "NodeGroup")
		os.Exit(1)
	}
	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting cluster scaler manager")
	scalerCtx, scalerCancel := context.WithCancel(context.Background())
	defer scalerCancel()
	scalerMgmtClient, err := dynamic.NewForConfig(managementConfig)
	if err != nil {
		setupLog.Error(err, "unable to create scaler management client")
		os.Exit(1)
	}
	scalerMgr := scalers.NewClusterScalerManager(
		scalerCtx,
		ctrlConfig.ScalerIntervalSeconds,
		scalerMgmtClient,
	)
	go scalerMgr.ScaleForever()

	setupLog.Info("starting controller manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
