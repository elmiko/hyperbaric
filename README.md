# Hyperbaric
A pressure-based node autoscaler for Kubernetes Cluster API.

## Description
Hyperbaric is a node autoscaler built around the concept of observing resource usage
within a cluster to inform scaling decisions. For example, Hyperbaric might be
configured to add nodes to the cluster when the CPU usage goes above 80%, and remove
nodes when the usage goes under 50%.

## Getting Started
Kubebuilder protocols are in effect, the usual commands work. Try `make help`
for inspiration.

This project is in early alpha state, check back frequently for updates.
