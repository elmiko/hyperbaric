/*
   Copyright 2023 Michael McCune.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package helpers

import (
	"crypto/rand"
	"fmt"
	"math/big"

	"gitlab.com/elmiko/hyperbaric/pkg/api/v1alpha1"
)

// NodeGroupBuilder is for creating NodeGroup objects using a fluent builder pattern.
type NodeGroupBuilder struct {
	nodegroup v1alpha1.NodeGroup
}

// NewNodeGroupBuilder returns a new builder with an empty NodeGroup.
func NewNodeGroupBuilder() *NodeGroupBuilder {
	ng := NodeGroupBuilder{}

	return &ng
}

func (ngb *NodeGroupBuilder) Build() *v1alpha1.NodeGroup {
	return ngb.nodegroup.DeepCopy()
}

func (ngb *NodeGroupBuilder) WithName(name string) *NodeGroupBuilder {
	ngb.nodegroup.Name = name

	return ngb
}

func (ngb *NodeGroupBuilder) WithRandomName() *NodeGroupBuilder {
	max := big.NewInt(99999)
	i, _ := rand.Int(rand.Reader, max)
	ngb.nodegroup.Name = fmt.Sprintf("node-group-%05d", i)
	return ngb
}

func (ngb *NodeGroupBuilder) WithNamespace(namespace string) *NodeGroupBuilder {
	ngb.nodegroup.Namespace = namespace

	return ngb
}

func (ngb *NodeGroupBuilder) WithScalableResourceRef(name string, group string, version string, resource string) *NodeGroupBuilder {
	ngb.nodegroup.Spec.ScalableResourceRef.Name = name
	ngb.nodegroup.Spec.ScalableResourceRef.Group = group
	ngb.nodegroup.Spec.ScalableResourceRef.Version = version
	ngb.nodegroup.Spec.ScalableResourceRef.Resource = resource

	return ngb
}
