/*
Copyright 2023 Michael McCune.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package scalers

import (
	"sync"

	"k8s.io/apimachinery/pkg/runtime/schema"
)

var (
	scalerMap = sync.Map{}
)

// Registers a scaler implementation with the scaler mapper, scalers are
// responsible for interacting with a specific resource type to perform
// resource querying and scaling operations.
func registerScaler(gvr schema.GroupVersionResource, scaler Scaler) {
	scalerMap.Store(gvr.String(), scaler)
}

// scalerForGVR returns a Scaler and boolean for a given GroupVersionResource,
// returning nil and false if not found.
func scalerForGVR(gvr schema.GroupVersionResource) (Scaler, bool) {
	return scalerMap.Load(gvr.String())
}
