/*
   Copyright 2022 Michael McCune.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package scalers

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	"gitlab.com/elmiko/hyperbaric/pkg/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/tools/cache"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	resourceNodeGroup = "nodegroups"
)

var (
	gvrNodeGroup = v1alpha1.GroupVersion.WithResource(resourceNodeGroup)
)

// ClusterScalerManager is responsible for watching scalable resources
// within a cluster and managing the associated scalers.
type ClusterScalerManager struct {
	stopChannel               context.Context
	intervalSeconds           int64
	managementClient          dynamic.Interface
	managementInformerFactory dynamicinformer.DynamicSharedInformerFactory
}

// NewClusterScalerManager creates new instances of ClusterScalerManager.
func NewClusterScalerManager(
	stopChannel context.Context,
	intervalSeconds int64,
	managementClient dynamic.Interface,
) ClusterScalerManager {
	csm := ClusterScalerManager{
		stopChannel:      stopChannel,
		intervalSeconds:  intervalSeconds,
		managementClient: managementClient,
		managementInformerFactory: dynamicinformer.NewFilteredDynamicSharedInformerFactory(
			managementClient,
			0,
			metav1.NamespaceAll,
			nil),
	}
	return csm
}

// ScaleForever runs a continuous loop that will evaluate the scalable
// resources within a cluster and make scaling recommendations based
// on the results.
func (csm ClusterScalerManager) ScaleForever() {
	logger := log.FromContext(csm.stopChannel)

	logger.Info("Scanning cluster", "intervalSeconds", csm.intervalSeconds)
	defer logger.Info("Finished scanning cluster")

	for {
		select {
		case <-csm.stopChannel.Done():
			return
		default:
			csm.scaleOnce()
			time.Sleep(time.Duration(csm.intervalSeconds) * time.Second)
		}
	}
}

// getNodeGroups returns the list of available NodeGroup objects.
func (csm ClusterScalerManager) getNodeGroups() ([]v1alpha1.NodeGroup, error) {
	/*
		    TODO figure out how to make the informer work with envtest

				informer, err := csm.nodeGroupInformer()
				if err != nil {
					return nil, err
				}

				ngobjects, err := informer.Lister().List(labels.Nothing())
				if err != nil {
					return nil, err
				}
	*/

	ngo, err := csm.managementClient.Resource(gvrNodeGroup).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	ngobjects := ngo.Items

	nodegroups := make([]v1alpha1.NodeGroup, 0, len(ngobjects))
	for _, ng := range ngobjects {
		/*
			        TODO figure out how to make the informer work with envtest
						nodegroup, ok := ng.(*v1alpha1.NodeGroup)
						if !ok {
		*/
		var nodegroup v1alpha1.NodeGroup
		err := runtime.DefaultUnstructuredConverter.FromUnstructured(ng.Object, &nodegroup)
		if err != nil {
			return nil, fmt.Errorf("unexpected nodegroups resource from lister, not %T", ng)
		}

		nodegroups = append(nodegroups, nodegroup)
	}

	return nodegroups, nil
}

// logger returns a context sensitive logging interface.
func (csm ClusterScalerManager) logger() logr.Logger {
	return log.FromContext(csm.stopChannel)
}

// nodeGroupInformer returns a GenericInformer configured for NodeGroups.
func (csm ClusterScalerManager) nodeGroupInformer() (informers.GenericInformer, error) {
	informer := csm.managementInformerFactory.ForResource(gvrNodeGroup)

	// just in case it hasn't been started, kick the factory, this is safe to call multiple times.
	csm.managementInformerFactory.Start(csm.stopChannel.Done())

	// wait for the informer cache to sync once
	csm.logger().Info("Syncing the NodeGroup informer cache")
	if !cache.WaitForCacheSync(csm.stopChannel.Done(), informer.Informer().HasSynced) {
		return nil, fmt.Errorf("Syncing caching on nodegroups failed")
	}

	return informer, nil
}

// scaleOnce perfoms a reconciliation of NodeGroups that its client is watching
// making scaling decision for each based on the resource usage and limits defined
// for the NodeGroup.
func (csm ClusterScalerManager) scaleOnce() {
	// get all NodeGroups in this namespace
	nodegroups, err := csm.getNodeGroups()
	if err != nil {
		csm.logger().Error(err, "Failed to get NodeGroups, returning scaleOnce early")
		return
	}

	csm.logger().V(0).Info("Processing NodeGroups", "number", len(nodegroups))
	for _, ng := range nodegroups {
		if updated, err := csm.processNodeGroup(&ng); err != nil {
			csm.logger().Error(err, "Failed to process NodeGroup, skipping", "NodeGroup", map[string]string{"name": ng.Name, "namespace": ng.Namespace})
			continue
		} else if updated {
			csm.logger().Info("Updating NodeGroup status", "NodeGroup", map[string]string{"name": ng.Name, "namespace": ng.Namespace})
			obj, err := ng.ToUnstructured()
			if err != nil {
				csm.logger().Error(err, "Failed to convert NodeGroup to Unstructured, skipping update", "NodeGroup", map[string]string{"name": ng.Name, "namespace": ng.Namespace})
				continue
			}

			if _, err := csm.managementClient.Resource(gvrNodeGroup).Namespace(ng.Namespace).UpdateStatus(csm.stopChannel, obj, metav1.UpdateOptions{}); err != nil {
				csm.logger().Error(err, "Failed to update NodeGroup", "NodeGroup", map[string]string{"name": ng.Name, "namespace": ng.Namespace})
				continue
			}
			csm.logger().V(0).Info("Updating NodeGroup", "name", ng.Name, "obj", obj)
			_, err = csm.managementClient.Resource(gvrNodeGroup).Namespace(ng.Namespace).UpdateStatus(csm.stopChannel, obj, metav1.UpdateOptions{})
			if err != nil {
				csm.logger().Error(err, "failed to update NodeGroup")
			}
		}
	}
}

// processNodeGroup will assess a node group to determine what action should be taken on this
// scaling loop. This function can mutate the target NodeGroup, it will return true if the object
// should be updated.
func (csm ClusterScalerManager) processNodeGroup(target *v1alpha1.NodeGroup) (bool, error) {
	if !target.HasReadyCondition() {
		csm.logger().Info("Skipping NodeGroup, does not have ready condition yet", "NodeGroup", map[string]string{"name": target.Name, "namespace": target.Namespace})
		return false, nil
	}

	// target node group can be scaled, check its resource usage against its thresholds and make a decision
	if target.IsReadyForScaling() {
		// TODO add scaling logic here
		// for each inititialized do:
		//   calculate usage resource usage
		//   calculate available capacity
		//   calculate upcoming capacity
		//   compare usage to capacity
		//   if beyond limit, need to check to see if it has been beyond limit for longer than desired (configurable time)
		//   	there should be some way to check the nodegroup for last update, not sure if condition fits
		//   make scaling decisoins based on previous calculation
		return false, nil
	}

	// target node group is uninitialized, examine its spec and initialize if possible
	if target.IsUninitialized() {
		if _, ok := scalerForGVR(target.Spec.ScalableResourceRef.GroupVersionResource()); !ok {
			// no scaler found for this reference, mark the node group as not ready
			csm.logger().V(0).Info("No scaler found for NodeGroup", "name", target.Name)
			target.SetReadyCondition(
				metav1.ConditionFalse,
				v1alpha1.ReasonNodeGroupNoKnownScaler,
				"No known scaler for this scalable resource reference")

			return true, nil
		}

		csm.logger().V(0).Info("Found scaler for NodeGroup", "name", target.Name)
		// scaler found, set the node group as ready for scaling
		target.SetReadyCondition(
			metav1.ConditionTrue,
			v1alpha1.ReasonNodeGroupReadyForScaling,
			v1alpha1.MessageNodeGroupReadyForScaling)

		return true, nil
	}

	return false, nil
}
