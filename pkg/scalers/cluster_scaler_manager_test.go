/*
   Copyright 2023 Michael McCune.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package scalers

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/elmiko/hyperbaric/pkg/api/v1alpha1"
	"gitlab.com/elmiko/hyperbaric/pkg/controllers"
	"gitlab.com/elmiko/hyperbaric/pkg/helpers"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	ctrl "sigs.k8s.io/controller-runtime"
)

type FakeScaler struct{}

var _ = Describe("ClusterScalerManager", func() {
	var (
		mgrCtx    context.Context
		mgrCancel context.CancelFunc
		mgrDone   chan struct{}
		mgr       ctrl.Manager
		csm       ClusterScalerManager
	)

	const (
		timeout10s    = time.Second * 10
		interval250ms = time.Millisecond * 250

		NodeGroupName      = "test-nodegroup"
		NodeGroupNamespace = "default"
	)

	BeforeEach(func() {
		By("Setting up a manager and controller")
		var err error
		mgr, err = ctrl.NewManager(cfg, ctrl.Options{
			Scheme:             testScheme,
			MetricsBindAddress: "0",
			Port:               testEnv.WebhookInstallOptions.LocalServingPort,
			Host:               testEnv.WebhookInstallOptions.LocalServingHost,
			CertDir:            testEnv.WebhookInstallOptions.LocalServingCertDir,
		})
		Expect(err).ToNot(HaveOccurred(), "Manager should be able to be created")

		reconciler := &controllers.NodeGroupReconciler{
			Client: mgr.GetClient(),
			Scheme: testScheme,
		}
		Expect(reconciler.SetupWithManager(mgr)).To(Succeed(), "Reconciler should be able to setup with Manager")

		mgrCtx, mgrCancel = context.WithCancel(context.Background())
		mgrDone = make(chan struct{})

		By("Creating a ClusterScalerManager")
		scalerMgmtClient, err := dynamic.NewForConfig(cfg)
		Expect(err).ToNot(HaveOccurred(), "Unexpected error creating the dynamic client")
		Expect(scalerMgmtClient).ToNot(BeNil(), "Scaler Management Client should not be nil")
		csm = NewClusterScalerManager(
			mgrCtx,
			15,
			scalerMgmtClient,
		)

		By("Starting the manager")
		go func() {
			defer GinkgoRecover()
			defer close(mgrDone)

			Expect(mgr.Start(mgrCtx)).To(Succeed())
		}()
	}, OncePerOrdered)

	AfterEach(func() {
		By("Stopping the manager")
		mgrCancel()
		// Wait for the mgrDone to be closed, which will happen once the mgr has stopped
		<-mgrDone

	}, OncePerOrdered)

	Context("When running scaleOnce with an uninitialized NodeGroup", func() {
		It("sets Ready condition to false when there is no known scaler for the ScalableResourceRef", func() {
			ctx := mgrCtx
			By("Creating a new NodeGroup")
			nodeGroup := helpers.NewNodeGroupBuilder().
				WithRandomName().
				WithNamespace(NodeGroupNamespace).
				WithScalableResourceRef("fake-thing", "fake.some.thing", "vWhat1", "fakethings").
				Build()

			Expect(k8sClient.Create(ctx, nodeGroup)).To(Succeed(), "Unexpected API error creating the NodeGroup")
			defer func() {
				Expect(k8sClient.Delete(ctx, nodeGroup)).To(Succeed(), "Unexpected API error deleting the NodeGroup")
			}()

			nodeGroupLookupKey := types.NamespacedName{Name: nodeGroup.Name, Namespace: NodeGroupNamespace}

			var readyCondition *metav1.Condition
			GetStatus := func(c *metav1.Condition) metav1.ConditionStatus { return c.Status }
			GetReason := func(c *metav1.Condition) string { return c.Reason }
			By("Waiting for the ready condition to be added in false and uninitialized state")
			Eventually(func() *metav1.Condition {
				createdNodeGroup := &v1alpha1.NodeGroup{}
				err := k8sClient.Get(ctx, nodeGroupLookupKey, createdNodeGroup)
				if err != nil {
					return nil
				}

				readyCondition = meta.FindStatusCondition(createdNodeGroup.Status.Conditions, v1alpha1.ConditionNodeGroupReady)
				if readyCondition != nil {
					return readyCondition
				}

				return nil
			}, timeout10s, interval250ms).Should(SatisfyAll(
				Not(BeNil()),
				WithTransform(GetStatus, Equal(metav1.ConditionFalse)),
				WithTransform(GetReason, Equal(v1alpha1.ReasonNodeGroupUninitialized)),
			), "Timed out waiting for the NodeGroup controller to add the Ready condition")

			By("Running scaleOnce")
			csm.scaleOnce()

			By("Waiting for the ready condition to be updated into no known scaler state")
			Eventually(func() *metav1.Condition {
				createdNodeGroup := &v1alpha1.NodeGroup{}
				err := k8sClient.Get(ctx, nodeGroupLookupKey, createdNodeGroup)
				if err != nil {
					return nil
				}

				readyCondition = meta.FindStatusCondition(createdNodeGroup.Status.Conditions, v1alpha1.ConditionNodeGroupReady)
				if readyCondition != nil {
					return readyCondition
				}

				return nil
			}, timeout10s, interval250ms).Should(SatisfyAll(
				Not(BeNil()),
				WithTransform(GetStatus, Equal(metav1.ConditionFalse)),
				WithTransform(GetReason, Equal(v1alpha1.ReasonNodeGroupNoKnownScaler)),
			), "Timed out waiting for the NodeGroup controller to update the Ready condition")
		})

		It("sets Ready condition to true when there is a known scaler for the ScalableResourceRef", func() {
			ctx := mgrCtx
			By("Creating a new NodeGroup")
			nodeGroup := helpers.NewNodeGroupBuilder().
				WithRandomName().
				WithNamespace(NodeGroupNamespace).
				WithScalableResourceRef("fake-thing", "fake.some.thing", "vWhat2", "fakethings").
				Build()

			Expect(k8sClient.Create(ctx, nodeGroup)).To(Succeed(), "Unexpected API error creating the NodeGroup")
			defer func() {
				Expect(k8sClient.Delete(ctx, nodeGroup)).To(Succeed(), "Unexpected API error deleting the NodeGroup")
			}()

			nodeGroupLookupKey := types.NamespacedName{Name: nodeGroup.Name, Namespace: NodeGroupNamespace}

			var readyCondition *metav1.Condition
			GetStatus := func(c *metav1.Condition) metav1.ConditionStatus { return c.Status }
			GetReason := func(c *metav1.Condition) string { return c.Reason }
			By("Waiting for the ready condition to be added in false and uninitialized state")
			Eventually(func() *metav1.Condition {
				createdNodeGroup := &v1alpha1.NodeGroup{}
				err := k8sClient.Get(ctx, nodeGroupLookupKey, createdNodeGroup)
				if err != nil {
					return nil
				}

				readyCondition = meta.FindStatusCondition(createdNodeGroup.Status.Conditions, v1alpha1.ConditionNodeGroupReady)
				if readyCondition != nil {
					return readyCondition
				}

				return nil
			}, timeout10s, interval250ms).Should(SatisfyAll(
				Not(BeNil()),
				WithTransform(GetStatus, Equal(metav1.ConditionFalse)),
				WithTransform(GetReason, Equal(v1alpha1.ReasonNodeGroupUninitialized)),
			), "Timed out waiting for the NodeGroup controller to add the Ready condition")

			By("Creating a fake scaler")
			fgvr := schema.GroupVersionResource{Group: "fake.some.thing", Version: "vWhat2", Resource: "fakethings"}
			registerScaler(fgvr, FakeScaler{})

			By("Running scaleOnce")
			csm.scaleOnce()

			By("Waiting for the ready condition to be updated into true state")
			Eventually(func() *metav1.Condition {
				createdNodeGroup := &v1alpha1.NodeGroup{}
				err := k8sClient.Get(ctx, nodeGroupLookupKey, createdNodeGroup)
				if err != nil {
					return nil
				}

				readyCondition = meta.FindStatusCondition(createdNodeGroup.Status.Conditions, v1alpha1.ConditionNodeGroupReady)
				if readyCondition != nil {
					return readyCondition
				}

				return nil
			}, timeout10s, interval250ms).Should(SatisfyAll(
				Not(BeNil()),
				WithTransform(GetStatus, Equal(metav1.ConditionTrue)),
				WithTransform(GetReason, Equal(v1alpha1.ReasonNodeGroupReadyForScaling)),
			), "Timed out waiting for the NodeGroup controller to update the Ready condition")
		})
	})
})
