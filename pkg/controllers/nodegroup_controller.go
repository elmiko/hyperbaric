/*
   Copyright 2022 Michael McCune.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	errorutils "k8s.io/apimachinery/pkg/util/errors"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	"gitlab.com/elmiko/hyperbaric/pkg/api/v1alpha1"
)

const (
	// nodeGroupFinalizer is the finalizer used by Hyperbaric to ensure
	// that a NodeGroup is not deleted before it has been removed from the
	// list of watched groups by the scalers.
	nodeGroupFinalizer = "nodegroup.autoscaling.elmiko.dev"
)

// NodeGroupReconciler reconciles a NodeGroup object
type NodeGroupReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=autoscaling.elmiko.dev,resources=nodegroups,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=autoscaling.elmiko.dev,resources=nodegroups/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=autoscaling.elmiko.dev,resources=nodegroups/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the NodeGroup object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.13.0/pkg/reconcile
func (r *NodeGroupReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx, "namespace", req.Namespace, "name", req.Name)

	logger.Info("Reconciling NodeGroup")
	defer logger.Info("Finished reconciling NodeGroup")

	ng := &v1alpha1.NodeGroup{}
	ngKey := client.ObjectKey{Namespace: req.Namespace, Name: req.Name}

	// Fetch the NodeGroup
	if err := r.Get(ctx, ngKey, ng); apierrors.IsNotFound(err) {
		logger.Info("NodeGroup not found, probably deleted", "nodegroup", req.Name)

		return ctrl.Result{}, nil
	} else if err != nil {
		return ctrl.Result{}, fmt.Errorf("unable to fetch NodeGroup: %w", err)
	}

	result, err := r.reconcile(ctx, logger, ng)
	if err != nil {
		return ctrl.Result{}, fmt.Errorf("error reconciling NodeGroup: %w", err)
	}

	return result, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *NodeGroupReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha1.NodeGroup{}).
		Complete(r)
}

// ensureFinalizer adds the NodeGroup finalizer if it does not already exist.
func (r *NodeGroupReconciler) ensureFinalizer(ctx context.Context, logger logr.Logger, ng *v1alpha1.NodeGroup) (bool, error) {
	// check to see if the finalizer needs to be added
	for _, finalizer := range ng.GetFinalizers() {
		if finalizer == nodeGroupFinalizer {
			logger.Info("Finalizer already present on NodeGroup", "nodegroup", ng.Name)
			return false, nil
		}
	}

	logger.Info("Adding finalizer to NodeGroup", "nodegroup", ng.Name)
	ng.SetFinalizers(append(ng.GetFinalizers(), nodeGroupFinalizer))
	if err := r.Client.Update(ctx, ng); err != nil {
		return false, fmt.Errorf("Error updating NodeGroup: %w", err)
	}

	return true, nil
}

// reconcile performs the main business logic for processing NodeGroups.
func (r *NodeGroupReconciler) reconcile(ctx context.Context, logger logr.Logger, ng *v1alpha1.NodeGroup) (ctrl.Result, error) {
	// If the NodeGroup is being deleted there is some bookkeeping that needs to be done
	// instead of the normal reconcile flow.
	if ng.GetDeletionTimestamp() != nil {
		return r.reconcileDelete(ctx, logger, ng)
	}

	var errs []error

	result, err := r.reconcileCreateOrUpdate(ctx, logger, ng)
	if err != nil {
		errs = append(errs, fmt.Errorf("error reconciling node group: %w", err))
	}
	if result.Requeue {
		return result, nil
	}

	if err = r.updateNodeGroupStatus(ctx, logger, ng); err != nil {
		errs = append(errs, fmt.Errorf("error updating status: %w", err))
	}

	if len(errs) > 0 {
		return ctrl.Result{}, errorutils.NewAggregate(errs)
	}

	return result, nil
}

// reconcileDelete ensures that the backing resource is no longer watched by the interval scanner.
func (r *NodeGroupReconciler) reconcileDelete(ctx context.Context, logger logr.Logger, ng *v1alpha1.NodeGroup) (ctrl.Result, error) {
	logger.Info("Processing NodeGroup delete", "nodegroup", ng.Name)

	finalizerUpdated := controllerutil.RemoveFinalizer(ng, nodeGroupFinalizer)
	if finalizerUpdated {
		logger.Info("Removing finalizer from NodeGroup", "nodegroup", ng.Name)
		if err := r.Update(ctx, ng); err != nil {
			return ctrl.Result{}, fmt.Errorf("error updating NodeGroup: %w", err)
		}
	}
	return ctrl.Result{}, nil
}

// reconcileCreateOrUpdate updates the list of watched groups by the interval scanner
func (r *NodeGroupReconciler) reconcileCreateOrUpdate(ctx context.Context, logger logr.Logger, ng *v1alpha1.NodeGroup) (ctrl.Result, error) {
	logger.Info("Processing NodeGroup create or update", "nodegroup", ng.Name)

	if updatedFinalizer, err := r.ensureFinalizer(ctx, logger, ng); err != nil {
		return ctrl.Result{}, fmt.Errorf("error adding finalizer: %w", err)
	} else if updatedFinalizer {
		return ctrl.Result{Requeue: true}, nil
	}

	return ctrl.Result{}, nil
}

// updateNodeGroupStatus ensures that the NodeGroup status is up to date with
// the cluster after reconciling.
func (r *NodeGroupReconciler) updateNodeGroupStatus(ctx context.Context, logger logr.Logger, ng *v1alpha1.NodeGroup) error {
	needsUpdate := false
	if !ng.HasReadyCondition() {
		// node group does not have a ready condition, add it
		readyCondition := metav1.Condition{
			Type:               v1alpha1.ConditionNodeGroupReady,
			Status:             metav1.ConditionFalse,
			Reason:             v1alpha1.ReasonNodeGroupUninitialized,
			ObservedGeneration: ng.Generation,
		}

		meta.SetStatusCondition(&ng.Status.Conditions, readyCondition)
		needsUpdate = true
	}

	if needsUpdate {
		logger.Info("Updating node group status", "nodegroup", ng.Name)
		if err := r.Status().Update(ctx, ng); err != nil {
			return fmt.Errorf("failed to update status for node group object: %w", err)
		}
	}

	return nil
}
