/*
   Copyright 2022 Michael McCune.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package controllers

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"

	"gitlab.com/elmiko/hyperbaric/pkg/api/v1alpha1"
	"gitlab.com/elmiko/hyperbaric/pkg/helpers"
)

var _ = Describe("With NodeGroup controller", func() {
	var (
		mgrCancel context.CancelFunc
		mgrDone   chan struct{}
		mgr       ctrl.Manager
	)

	const (
		timeout10s    = time.Second * 10
		interval250ms = time.Millisecond * 250

		NodeGroupName      = "test-nodegroup"
		NodeGroupNamespace = "default"
	)

	BeforeEach(func() {
		By("Setting up a manager and controller")
		var err error
		mgr, err = ctrl.NewManager(cfg, ctrl.Options{
			Scheme:             testScheme,
			MetricsBindAddress: "0",
			Port:               testEnv.WebhookInstallOptions.LocalServingPort,
			Host:               testEnv.WebhookInstallOptions.LocalServingHost,
			CertDir:            testEnv.WebhookInstallOptions.LocalServingCertDir,
		})
		Expect(err).ToNot(HaveOccurred(), "Manager should be able to be created")

		reconciler := &NodeGroupReconciler{
			Client: mgr.GetClient(),
			Scheme: testScheme,
		}
		Expect(reconciler.SetupWithManager(mgr)).To(Succeed(), "Reconciler should be able to setup with Manager")

		By("Starting the manager")
		var mgrCtx context.Context
		mgrCtx, mgrCancel = context.WithCancel(context.Background())
		mgrDone = make(chan struct{})

		go func() {
			defer GinkgoRecover()
			defer close(mgrDone)

			Expect(mgr.Start(mgrCtx)).To(Succeed())
		}()
	}, OncePerOrdered)

	AfterEach(func() {
		By("Stopping the manager")
		mgrCancel()
		// Wait for the mgrDone to be closed, which will happen once the mgr has stopped
		<-mgrDone

	}, OncePerOrdered)

	Context("When creating a NodeGroup", func() {
		It("sets Ready condition to false and uninitialized", func() {
			ctx := context.Background()
			By("By creating a new NodeGroup")
			nodeGroup := helpers.NewNodeGroupBuilder().
				WithRandomName().
				WithNamespace(NodeGroupNamespace).
				WithScalableResourceRef("fake-thing", "fake.some.thing", "vWhat1", "fakethings").
				Build()

			Expect(k8sClient.Create(ctx, nodeGroup)).To(Succeed(), "Unexpected API error creating the NodeGroup")

			nodeGroupLookupKey := types.NamespacedName{Name: nodeGroup.Name, Namespace: NodeGroupNamespace}
			createdNodeGroup := &v1alpha1.NodeGroup{}

			var readyCondition *metav1.Condition
			By("Waiting for the ready condition to be added")
			Eventually(func() bool {
				err := k8sClient.Get(ctx, nodeGroupLookupKey, createdNodeGroup)
				if err != nil {
					return false
				}
				readyCondition = meta.FindStatusCondition(createdNodeGroup.Status.Conditions, v1alpha1.ConditionNodeGroupReady)
				if readyCondition != nil {
					return true
				}
				return false
			}, timeout10s, interval250ms).Should(BeTrue())

			Expect(readyCondition.Status).To(Equal(metav1.ConditionFalse))
			Expect(readyCondition.Reason).To(Equal(v1alpha1.ReasonNodeGroupUninitialized))
		})
	})
})
