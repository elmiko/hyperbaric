/*
Copyright 2023 Michael McCune.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package v1alpha1

import (
	"reflect"

	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	runtime "k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const (
	// Ready condition type for NodeGroups
	ConditionNodeGroupReady = "Ready"

	// ReasonNodeGroupReadyForScaling denotes that the NodeGroup is capable of performing
	// scaling operations requested by the scaler.
	ReasonNodeGroupReadyForScaling = "ReadyForScaling"

	// ReasonNodeGroupUninitialized denotes that the NodeGroup has not yet been initialized.
	ReasonNodeGroupUninitialized = "Uninitialized"

	// ReasonNodeGroupNoKmownScaler denotes that there is no known scaler for the scalabler resource
	// reference in the NodeGroup.
	ReasonNodeGroupNoKnownScaler = "NoKnownScaler"

	// MessageNodeGroupReadyForScaling is the message text to use with the ReasonNodeGroupReadyForScaling
	MessageNodeGroupReadyForScaling = "NodeGroup is ready for scaling activity."
)

// GetReadyCondition returns a pointer to the Ready condition for the NodeGroup
// or nil if it is not present.
func (ng *NodeGroup) GetReadyCondition() *metav1.Condition {
	for i, c := range ng.Status.Conditions {
		if c.Type == ConditionNodeGroupReady {
			return &ng.Status.Conditions[i]
		}
	}

	return nil
}

func (sr ScalableResourceReference) GroupVersionResource() schema.GroupVersionResource {
	return schema.GroupVersionResource{
		Group:    sr.Group,
		Version:  sr.Version,
		Resource: sr.Resource,
	}
}

func (ng NodeGroup) HasReadyCondition() bool {
	if c := ng.GetReadyCondition(); c != nil {
		return true
	}

	return false
}

func (ng NodeGroup) IsReadyForScaling() bool {
	for _, c := range ng.Status.Conditions {
		if c.Type == ConditionNodeGroupReady &&
			c.Status == metav1.ConditionTrue &&
			c.Reason == ReasonNodeGroupReadyForScaling {
			return true
		}
	}

	return false
}

func (ng NodeGroup) IsUninitialized() bool {
	for _, c := range ng.Status.Conditions {
		if c.Type == ConditionNodeGroupReady &&
			c.Status == metav1.ConditionFalse &&
			c.Reason == ReasonNodeGroupUninitialized {
			return true
		}
	}

	return false
}

// SetReadyCondition sets the ready condition to the specified status, reason, and message, it
// will create the condition if it does not exist. It returns a pointer to the Condition.
func (ng *NodeGroup) SetReadyCondition(status metav1.ConditionStatus, reason string, message string) *metav1.Condition {
	rc := ng.GetReadyCondition()
	if rc == nil {
		rc = &metav1.Condition{}
		rc.Type = ConditionNodeGroupReady
	}
	rc.Status = status
	rc.Reason = reason
	rc.Message = message

	meta.SetStatusCondition(&ng.Status.Conditions, *rc)
	return rc
}

func (ng *NodeGroup) ToUnstructured() (*unstructured.Unstructured, error) {
	obj, err := runtime.DefaultUnstructuredConverter.ToUnstructured(ng)
	if err != nil {
		return nil, err
	}

	u := unstructured.Unstructured{
		Object: obj,
	}

	return &u, nil
}

func NodeGroupFromUnstructured(u *unstructured.Unstructured) (*NodeGroup, error) {
	newObj := reflect.New(reflect.TypeOf(NodeGroup{})).Interface()
	if err := runtime.DefaultUnstructuredConverter.FromUnstructured(u.Object, newObj); err != nil {
		return nil, err
	} else {
		return newObj.(*NodeGroup), nil
	}
}
