/*
   Copyright 2022 Michael McCune.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	cfg "sigs.k8s.io/controller-runtime/pkg/config/v1alpha1"
)

// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

//+kubebuilder:object:root=true

// HyperbaricConfig is the Schema for the hyperbaricconfigs API
// This API type is only used for the configuration file, it is not serialized as a CRD.
type HyperbaricConfig struct {
	metav1.TypeMeta `json:",inline"`

	// ControllerManagerConfigurationSpec returns the configurations for controllers
	cfg.ControllerManagerConfigurationSpec `json:",inline"`

	// ScalerIntervalSeconds defines the interval period in seconds for evaluation of
	// scaling opportunities within the cluster.
	ScalerIntervalSeconds int64 `json:"scalerIntervalSeconds,omitempty"`

	// ManagementKubeconfig defines the path to the Kubeconfig file for the cluster to
	// watch for NodeGroups and scalable resources.
	ManagementKubeconfig string `json:"managementKubeconfig,omitempty"`

	// WorkloadKubeconfig defines the path to the Kubeconfig file for the cluster to
	// watch for Pods and Nodes to evaluate resource levels.
	WorkloadKubeconfig string `json:"workloadKubeconfig,omitempty"`
}

func init() {
	SchemeBuilder.Register(&HyperbaricConfig{})
}
